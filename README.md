# Workshop #1

Hi! This is the first  **WorkShop** for the course. 
In this repo you'll find a `java` project created in `Intelli J`  with some simple and funny exercises that you can probe through the automated tests, included in the project.

# Instructions
To enjoy this workshop, be sure you have a:

>GitLab account.

>Intelli J IDEA installed.

>Internet.

>CLI

>## Fork 
In the right corner, you can found the `Fork` option button. Click it.

####       Now you have your own copy of the project. 

>##  Clone
In your new repo, click in the `Clone` option button and follow the instructions or remember the lessons in classroom.

#### Now you have in your Local repo, a copy of the workshop, connected with your online repo. 

>## Open in IntelliJ
In your IDE, import the project.

>## ***Solving the exercises***
You'll find some packages, divided in two:
#### -Main
This is the package with three files called `classes`
Here, you should solve the exercises proposed.
#### -Test
This is the space where we can check the correct use of your code.
Chill out about it.


## Present your work

When you complete your work, commit/push to your GitLab repo, and make a merge request to my repo.

## What is the qualification mode?
In class, will run the test for your code and it must to pass all of them.



